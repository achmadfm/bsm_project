
<div class="header-row pt-3">
    <div class="header-search d-none d-md-block">
          <form id="searchForm" action="<?php echo base_url().'berita/search'?>" method="get">
            <div class="input-group">
              <input type="text" class="form-control" name="textcari" id="q" placeholder="Search..." required>
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
        </div>
	<nav class="header-nav-top">
		<ul class="nav nav-pills">
			<li class="nav-item d-none d-sm-block">
				<a class="nav-link" href="https://bosowafoundation.org/" target="_blank"><i class="fa fa-globe"></i> Bosowa Foundation</a>
			</li>
			<li class="nav-item d-none d-sm-block">
				<a class="nav-link" href="https://bogor.bosowaschool.sch.id/" target="_blank"><i class="fa fa-globe"></i> Bosowa Bina Insani Bogor</a>
			</li>
			<li class="nav-item d-none d-sm-block">
				<a class="nav-link" href="https://cilegon.bosowaschool.sch.id/" target="_blank"><i class="fa fa-globe"></i> Bosowa Al-Azhar Cilegon</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="https://www.librarybsm.bosowaschool.sch.id/" target="_blank"><i class="fa fa-book"></i> Library Bosowa School</a>
			</li>
		</ul>
	</nav>
</div>
