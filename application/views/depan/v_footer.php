<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="footer-ribbon">
				<span style="font-family:Tahoma;">Bosowa School Makassar</span>
			</div>
			<div class="col-lg-3">
				<div class="contact-details">
				    <br>
    			        <h4><strong>PG - TK - SD</strong></h4>
    					<a target="_blank" href="https://www.google.com/maps/place/Sekolah+Alam+Bosowa/@-5.1701387,119.3943366,17z/data=!4m12!1m6!3m5!1s0x2dbee2a7d3bd8a69:0xe6fbf6d9f74bcd7a!2sSekolah+Alam+Bosowa!8m2!3d-5.1701387!4d119.3965253!3m4!1s0x2dbee2a7d3bd8a69:0xe6fbf6d9f74bcd7a!8m2!3d-5.1701387!4d119.3965253"><p><i class="fa fa-map-marker"></i> <strong></strong> Jl. Danau Tj. Bunga, Maccini Sombala, Tamalate, Makassar - 90224</p></a>
    			        
					<br>
					    <h4><strong>SMP - SMA</strong></h4>
						<a target="_blank" href="https://www.google.com/maps/place/Bosowa+School+Makassar/@-5.1515943,119.4185027,17z/data=!3m1!4b1!4m5!3m4!1s0x2dbee2a7d50fd595:0xcfd7bad563becf25!8m2!3d-5.1515996!4d119.4206914"><p><i class="fa fa-map-marker"></i> <strong></strong> Jl. Lanto Dg. Pasewang Street Number 39-41, BMDI Building - Makassar - 90125</p></a>
					    
				</div>
			</div>
			<div class="col-sm-1">
			    
			</div>
			<div class="col-lg-3">
				<h5 class="mb-2">Call Us / Contact Persons</h5>
				<span class="phone">(0411) 855-123</span>
				<p class="mb-0">Eko Arianto : +62812-8120-1063<br>Farannisa : +62812-4243-9953 <br>Fachru Sawati : +62813-9888-8018 </p>
				<ul class="list list-icons list-icons-sm">
					<li><i class="fa fa-envelope"></i> <a href="#">info.bosowaschoolmakassar@gmail.com</a></li>
				</ul>
			</div>
			<div class="col-lg-2">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					<li class="social-icons-facebook"><a href="https://www.facebook.com/bosowa.bsm" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
					<li class="social-icons-instagram"><a href="https://www.instagram.com/bosowaschoolmks/?hl=en" target="_blank" title="Instagram"><i class="fa fa-instagram"></i></a></li>
					<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCC6J3lYTGN3xSzreVvwBOVg" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<div class="col">
					<p>© Copyright 2019. All Rights Reserved.</p>
				</div>
			</div>
		</div>
	</div>
</footer>
