<?php

class Insight extends CI_Controller{
    function __construct(){
        parent::__construct();
        if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
        $this->load->model('m_kategori');
        $this->load->model('m_artikel');
    }

    function index(){
        $x['data'] = $this->db->query("SELECT * FROM tbl_artikel WHERE publish_status='0'");
        $this->load->view('admin/v_art_publish',$x);
    }

    function publish(){
        $kode = htmlspecialchars($this->uri->segment(4),ENT_QUOTES);
        $this->db->query("UPDATE tbl_artikel SET publish_status='1' WHERE artikel_id='$kode'");
        echo $this->session->set_flashdata('msg','success');
        redirect('admin/insight');
    }
}